const restFul = require('node-restful')
const mongoose = restFul.mongoose

const todoSchema = new mongoose.Schema({
    descricao: { type: String, required: true},
    tarefaFeita: { type: Boolean, required: true, default: false},
    dataCriacaoTarefa: { type: Date, default: Date.now}

})

module.exports = restFul.model('Todo', todoSchema)